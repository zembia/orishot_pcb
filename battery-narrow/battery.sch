EESchema Schematic File Version 4
LIBS:battery-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:TestPoint TP2
U 1 1 5F4EB4E3
P 2925 1475
F 0 "TP2" H 2983 1638 50  0000 L CNN
F 1 "TestPoint" H 2983 1547 50  0000 L CNN
F 2 "zembia-footprints:Keystone-254" H 2983 1456 50  0000 L CNN
F 3 "~" H 3125 1475 50  0001 C CNN
F 4 "254" H 2925 1475 50  0001 C CNN "MPN"
	1    2925 1475
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP1
U 1 1 5F4EBBDA
P 1925 1550
F 0 "TP1" H 1983 1713 50  0000 L CNN
F 1 "TestPoint" H 1983 1622 50  0000 L CNN
F 2 "zembia-footprints:Keystone-254" H 1983 1531 50  0000 L CNN
F 3 "~" H 2125 1550 50  0001 C CNN
F 4 "254" H 1925 1550 50  0001 C CNN "MPN"
	1    1925 1550
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H2
U 1 1 5F4EBFB7
P 1925 1900
F 0 "H2" H 2025 1946 50  0000 L CNN
F 1 "MountingHole" H 2025 1855 50  0000 L CNN
F 2 "zembia-footprints:9774100360R" H 1925 1900 50  0001 C CNN
F 3 "~" H 1925 1900 50  0001 C CNN
F 4 "9774090360R" H 1925 1900 50  0001 C CNN "MPN"
	1    1925 1900
	-1   0    0    1   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H1
U 1 1 5F4EC21A
P 2925 1875
F 0 "H1" H 3025 1921 50  0000 L CNN
F 1 "MountingHole" H 3025 1830 50  0000 L CNN
F 2 "zembia-footprints:9774100360R" H 2925 1875 50  0001 C CNN
F 3 "~" H 2925 1875 50  0001 C CNN
F 4 "9774090360R" H 2925 1875 50  0001 C CNN "MPN"
	1    2925 1875
	-1   0    0    1   
$EndComp
Wire Wire Line
	1925 1550 1925 1800
Wire Wire Line
	2925 1475 2925 1775
$EndSCHEMATC
